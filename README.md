# tls-checker
## What is this?

A way to test and troubleshoot SSL/TLS connectivity between GitLab Runner and a given `$TARGET`.

## Requirements

- GitLab Runner docker or kubernetes executor installed on Linux machine, or GitLab.com-provided shared runner (Runner Cloud).
- Endpoint defined via $TARGET CI variable

## Instructions

Create a CI variable `TARGET` with the `https://` URL of your endpoint.

### Either: `include: remote`

1. Set TARGET variable to the endpoint where you'd like to check TLS/SSL certificate details.
2. `include:remote` the tls-checker `.gitlab-ci.yml`

  ```yaml
  include:
    - remote: "https://gitlab.com/gitlab-com/support/toolbox/tls-checker/-/raw/main/.gitlab-ci.yml"
  ```

### Or: Add CI job

Copy/paste the following yaml into an existing `.gitlab-ci.yml` file

```yaml
tls-checker:
  image: alpine:latest
  # variables:
  #   TARGET: "gitlab.example.com"
  script:
    - apk add openssl
    - echo | openssl s_client -servername $TARGET --connect $TARGET:443 --verify_hostname $TARGET --showcerts 
    - echo | openssl s_client -servername $TARGET --connect $TARGET:443 --verify_hostname $TARGET --showcerts > results.txt 2>&1 
  artifacts:
    paths:
      - results.txt
```

For more robust/detailed TLS/SSL cert analysis via GitLab CI job, see https://gitlab.com/greg/testssl .
